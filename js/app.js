window.onload = () => {

  console.log('Chrome extension loaded: Tri-hotel')

  setInterval(checkHotels, 500)

}

const checkHotels = () => {

  let hotelList = document.querySelectorAll('.hotel.item-order__list-item.js_co_item');
  hotelList.forEach(hotel => {
    button = hotel.querySelector('.btn.btn--deal.icon-bg-icn_arrow_deal.fl-trailing')
    if (button == undefined) return; // still not constructed
    button.textContent = 'Order now!';
    button.addEventListener('click', e => handleEvent(e, hotel))
  })

}

/**
 * Handle the button event
 * @param {event} e 
 * @param {HTMLnode} hotel 
 */
const handleEvent = (e, hotel) => {
  // e.preventDefault(); // Not necessary
  const hotelId = `?hotelName=` + hotelName(hotel)
  const stars = '&hotelStars=' + (hotelStars(hotel) || 0)
  const width = 1426;
  const height = 881;
  const dates = getDates();
  const url = chrome.extension.getURL("dist/index.html") + hotelId + stars + dates;
  popupCenter(url, 'Tomeu', width, height);
}

/**
 * Creates a poput and render it over center.
 * @param {string} url 
 * @param {string} title 
 * @param {integer} width 
 * @param {integer} height 
 */
const popupCenter = (url, title, width, height) => {
  const left = (screen.width / 2) - (width / 2);
  const top = (screen.height / 2) - (height / 2);
  
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
};

/**
 * Get current dates selected.
 */
const getDates = () => {
  
  // TIP: could be someting like this too:
  // window.location.href.split('?')[1].split('&')
  // but they could remove the url, gg wp

  // Only 2 elements, we should care about this
  // maybe there are not or they are empty...
  let dates = document.querySelectorAll('time'); 
  
  // First will be 'departure' and the second 'arrival'
  const keys = ['departure', 'arrival']
  
  return [...dates] 
    .map(x => x.textContent.trim().split(',')[1].trim()) // Ex: Sábado, 01.04.17
    .reduce((pv, cv, index) => {
      return `${pv}&${keys[index]}=${cv}` // Ex: &departure='2017-04-09'
    }, '')
}
 
/**
 * Returns (if exists) the name of hotel
 * @param {HTMLnode} hotelNode 
 */
const hotelName = (hotelNode) => {
  const nameNode = hotelNode.querySelector('.name__copytext.m-0.item__slideout-toggle')
  if (nameNode) return nameNode.textContent
  else return null
}

/**
 * Returns (if exists) the number of stars of hotel
 * @param {HTMLnode} hotelNode 
 */
const hotelStars = (hotelNode) => {
  const starNodes = hotelNode.querySelectorAll('.icon-ic.item__star')
  if (starNodes) return starNodes.length
  else return null
}